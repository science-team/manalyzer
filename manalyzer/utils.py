#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys, os
import logging

import numpy

##############
DTRS_NB = 24
EPSILON_ANGLE = 0.0001
###########################
def sign(number):return cmp(number,0)


###########################
def set_output_path(data_filename, working_path = './'):
    
    if not os.path.exists(working_path):
        print ('Path'+ working_path+ 'does not exist')
        sys.exit()
        
    output_path = os.path.join(working_path, 
                               os.path.basename(data_filename)[:-4]) 
                               
    if not os.path.basename(data_filename)[-4] == '.':
        output_path = os.path.join(working_path, 'tmp_output')
        print ('WARNING: the output directory is going to be'+ output_path)
        
    if not os.path.exists(output_path):
        os.mkdir(output_path)   
    
    return output_path


def clean_scan(rx2, dtrs_data, log_level=logging.INFO):
    
    # Erasing the lines corresponding to the same angle ---
    i = 0
    while abs(rx2[i+1] - rx2[i]) <= EPSILON_ANGLE:
        if i < rx2.size - 1:
            i += 1
            #print rx2[i+1]-rx2[i] # debug
        else:
            logging.error("  Invalid angle column")
            raise Exception("Invalid angle column")
            
    angle_startIndx = i
    if angle_startIndx != 0:
        logging.info('  angle_startIndx = '+ str(angle_startIndx)) # debug


    interrupted_scan = False

    i = rx2.size -1
    while abs(rx2[i] - rx2[i-1]) <= EPSILON_ANGLE:
        i -= 1

    angle_endIndx = i
    if rx2[i] == 0:
        angle_endIndx = i-1
        interrupted_scan = True
    
    if angle_endIndx != dtrs_data.shape[1]-1:
        logging.info('  angle_endIndx = '+ str(angle_endIndx) +' over '+ str(dtrs_data.shape[1]-1)) # debug
    # Erasing the lines corresponding to the same angle DONE ---

    
    rx2_clean = rx2[angle_startIndx:angle_endIndx+1]

##    average_step = numpy.mean(rx2[1:]-rx2[:-1])
##    uniform_step = (rx2[-1]-rx2[0])/len(rx2)
##    #if abs(average_step-uniform_step) > 
    
    dtrs_data_clean = dtrs_data[:, angle_startIndx:angle_endIndx+1]

    return rx2_clean, dtrs_data_clean, interrupted_scan
    
    

def read_data(abs_datapath, log_level=logging.WARNING):
    """
    Read data from a nxs or an ascii file;
    Remove the part of the curves where the motor was not moving;
    Return  rx2: a 1D numpy.array == rx2 values,
            dtrs_data: a nD numpy.array == measures from n dtrs.
    """
    
    if not os.path.exists(abs_datapath):
    #print ("Data file "+  data_abspath +" not found")
        raise Exception("Data file "+  abs_datapath +" not found")
    
    try:
    #if 1==1:
        if abs_datapath.endswith('nxs'):
            rx2, dtrs_data = read_nxs(abs_datapath)
        else:
            rx2, dtrs_data = read_ascii(abs_datapath)
    except: 
        raise Exception("Unexpected format in "+  abs_datapath)
        
    rx2, dtrs_data, interrupted_scan = clean_scan(rx2, dtrs_data, log_level)
        
    return rx2, dtrs_data, interrupted_scan
        
###########################
def read_nxs(nxs_filename):    
    import h5py
    
    with h5py.File(nxs_filename, 'r') as nxsfile:
        nxsID = list(nxsfile.keys())[0]  # num. du scan
    
        rx2 = nxsfile[nxsID+'/scan_data/actuator_1_1'][:]
        dtrs_data = numpy.array([nxsfile[nxsID+'/scan_data/data_{:0>2d}'.format(i)][:]
                                 for i in range(1, DTRS_NB+1)])
    return rx2, dtrs_data
    
    
###########################
def read_ascii(filename):
    """
    Read data from a .txt file extracted with nxextractor;
    """

    # reading the 1st column corresponding to the rx2 values 
    rx2 = numpy.loadtxt(filename, comments = '#', usecols = (0,), unpack=True) #  column
    #print rx2 # debug

    # reading DTRS_NB following columns corresponding to the intencities measured on DTRS_NB dtrs
    dtrs_data = numpy.loadtxt(filename, comments = '#', usecols = (range(1,DTRS_NB+1)),
                              unpack=True) #  columns
    #print 'dtrs_data.shape = ', dtrs_data.shape # debug

    return rx2, dtrs_data


def set_active_dtrs(filename, dtrs_nb=24):
    
    if not os.path.exists(filename):
        print ("File"+ filename +"not found")
        print ("Default: All detectors are active, the first one is the reference")
        dtrs_use = numpy.ones(dtrs_nb)
        dtrs_use[0] = 2
    else:
        dtrs_use = numpy.loadtxt(filename, comments = '#', usecols = (1,), 
                                 unpack=True) 
        if len(dtrs_use[dtrs_use == 2]) != 1:
            print ("Invalid config file"+ filename)
            sys.exit()
    #
    ref_dt = numpy.where(dtrs_use == 2)[0]+1
    active_dtrs = numpy.where(dtrs_use > 0)[0]+1

    return ref_dt, active_dtrs


###################
def get_round_step(step):

    demi_milli=0.0002
    nb_demilli=int(step/demi_milli+0.5)
    
    if abs(step-nb_demilli*demi_milli) < EPSILON_ANGLE:
        return nb_demilli*demi_milli

    print ("Warning by utils.get_round_step: check angular step")
    return step


