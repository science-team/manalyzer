#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys, os
#import subprocess as sp
#import signal
import time, datetime

import numpy
import scipy # import interpolate

from manalyzer import utils, linear_interpolation

###########################
EPSILON_ANGLE = 0.0001

HISTOGRAM_NBINS = 250
HISTO_BIN_SIZE = 1e-6

NONSENS_SHIFT_VAL = 1000


###################################################
def subtract_background(bg_filename, rx2, dtrs_data):

    if not os.path.exists(bg_filename):
        print ("Background file "+ os.path.abspath(bg_filename) +"not found")
        sys.exit()
        
    bg_rx2, bg_data = utils.reading_data(bg_filename)

    print (str(len(rx2), rx2[0], rx2[-1]))
    print (str(len(bg_rx2), bg_rx2[0], bg_rx2[-1]))
    
    '''
    if rx2.all() != bg_rx2.all():
        print ("Background is supposed to be measured \\\
        for the same rx2 values as the sample")
        sys.exit(0)

    return dtrs_data - bg_data 
    '''

    li = numpy.argmin( numpy.abs(rx2 - max(rx2[0],bg_rx2[0])) )  # left bound index
    if rx2[li] < bg_rx2[0]:
        li += 1
    ri = numpy.argmin( numpy.abs(rx2 - min(rx2[-1], bg_rx2[-1])) )  
    if rx2[ri] > bg_rx2[-1]:
        ri -= 1
        
    assert(ri-li > 5)  # a la louche

    for d in range(len(bg_data)):
        interp_bg = scipy.interpolate.interp1d(bg_rx2,  bg_data[d])
        dtrs_data[d][li:ri+1] -= interp_bg(rx2[li:ri+1])
        
    return rx2[li:ri+1], dtrs_data[:, li:ri+1]
    
###################
def get_round_step(step):

    demi_milli=0.0002
    nb_demilli=int(step/demi_milli+0.5)
    
    if abs(step-nb_demilli*demi_milli) < EPSILON_ANGLE:
        return nb_demilli*demi_milli

    print ("Warning by utils.get_round_step: check angular step")
    return step

#####################################################
def data_on_new_Xpoints(new_x_points, x_points, data):
    
    new_data = []
    
    margin = (new_x_points[-1]-new_x_points[0])/len(new_x_points)
    start_x = 0
    end_x = len(new_x_points)
    if x_points[0] - margin <= new_x_points[0] and new_x_points[0] < x_points[0]:
        start_x = 1
    if x_points[-1] < new_x_points[-1] and new_x_points[0] <= x_points[-1] + margin:
        end_x = len(new_x_points) -1
    
    for d in range(len(data)):
        
        new_data.append(linear_interpolation.interpolate(new_x_points[start_x:end_x], x_points, data[d]))
        
        if start_x == 1:
            new_data[d] = numpy.concatenate(([data[d,0]], new_data[d]))
        if end_x == len(new_x_points) -1:
            new_data[d] = numpy.concatenate((new_data[d], [data[d,-1]]))
            
    return new_data
    
##########################
def histogram(data, nbins):

    minVal = numpy.minimum.reduce(data)
    maxVal = numpy.maximum.reduce(data)

    bin_width = (maxVal-minVal)/nbins

    inf_bounds = minVal + bin_width*numpy.arange(nbins)
    sup_bounds = inf_bounds + bin_width

    h =  ((data[:, numpy.newaxis] > inf_bounds[numpy.newaxis,:]) &
            (data[:, numpy.newaxis] <= sup_bounds[numpy.newaxis,:]))

    x = 0.5*(inf_bounds + sup_bounds)

    return x, numpy.add.reduce(h)



##########################
def angular_step_stats(rx2, data_path = './'):
    
    rx2_steps = rx2[1:]-rx2[:-1]

    average_step = numpy.mean(rx2_steps)
    stdev_step = numpy.std(rx2_steps)

##    output_steps = open(data_path + 'angular_steps.dat', 'w')
##    for i in range (len(rx2_steps)): 
##        output_steps.write( str(i+1) + ' ' + str(rx2_steps[i]) + '\n')

    # ---------- histogram ---------------
    hist_nbins = HISTOGRAM_NBINS
    hist_nbins = int(((average_step + 3*stdev_step) - (average_step - 3*stdev_step))/HISTO_BIN_SIZE)
    
    steps_histogram = histogram(rx2_steps, hist_nbins)
    
    output_histogram = open(data_path + 'steps_histogram.dat', 'w')
    for i in range (hist_nbins): 
        output_histogram.write( str(steps_histogram[0][i]) + ' ' + str(steps_histogram[1][i]) + '\n')

##    print numpy.min(rx2_steps), numpy.argmin(rx2_steps)
##    print numpy.max(rx2_steps), numpy.argmax(rx2_steps)
##    print numpy.mean(rx2_steps)
##    print numpy.std(rx2_steps)

##    rx2_steps = numpy.delete(rx2_steps,numpy.argmin(rx2_steps))
##    rx2_steps = numpy.delete(rx2_steps,numpy.argmax(rx2_steps))
    
##    print 
##    print numpy.min(rx2_steps), numpy.argmin(rx2_steps)
##    print numpy.max(rx2_steps), numpy.argmax(rx2_steps)
##    print numpy.mean(rx2_steps)
##    print numpy.std(rx2_steps)

    return average_step, stdev_step

############################
class angular_zone_separator:
    """
    self.val is the angle limiting the angular zone seen by the detector self.dt 
    the detector whether starts (r_or_l == 'l') or stops (self.r_or_l == 'r') to "see"
    at the angle self.val 
    """
    def __init__(self, val, r_or_l, dt): 
        
        self.val = val
        self.r_or_l = r_or_l
        self.dt = dt

    def __repr__(self):

        return str(self.val) + ' ' + str(self.r_or_l) + ' ' + str(self.dt) + '\n'


##############################  
def get_dt_angles(rx2, shifts):
    """
    rx2 : numpy.array: motor position
    shifts : numpy.array: detectors shifts

    returns a dictionnary of the real detector rx angle: dtrs_angles
    
    This method uses NONSENS_SHIFT_VALUE to determine
    if the detector must be taken into account.
    each key of the dictionnay is a detector number.
    """
    dtrs_angles = {}

    for d in range(len(shifts)):
        ##        print d # debug
        ##        print abs(shifts[d]) # debug
        
        if abs(shifts[d]) != NONSENS_SHIFT_VAL:
            dtrs_angles[d+1] = rx2 - shifts[d]
            
## -------- to be deleted -------------
##        curr_dt_angles = numpy.zeros(len(rx2))
##        for i in range(len(rx2)):
##            curr_dt_angles[i] = rx2[i]-shifts[d]
            
##        dtrs_angles[d+1] = curr_dt_angles
## -------------------------------------
            
    return dtrs_angles


  
def get_dt_eqAngles_indxBounds(equidistant_angles, dt_angles):
    """
    equidistant_angles: whole range of the equidistant angles of all detectors
    dt_angles: dict with real detectors angles.

    return dt_eqAngles_indxBounds: a dictionnary containing the indexes
    of the angular bounds in the average_angles array for each detector
    """
    dt_eqAngles_indxBounds = {}
    
    valid_dtrs = list(dt_angles.keys())
    
    # need to sort the detectors (it seems that the dictionnaries are not sorted)
    valid_dtrs.sort()

    for dt in valid_dtrs[:-1]:
        #print dt # debug
        i = 0
        while equidistant_angles[i] <  dt_angles[dt][0]:
            i += 1
        lower_bound = i
        while equidistant_angles[i] <= dt_angles[dt][-1]:
            i += 1
        upper_bound = i-1
        
        dt_eqAngles_indxBounds[dt] = [lower_bound, upper_bound]

    i = 0
    while equidistant_angles[i] <  dt_angles[valid_dtrs[-1]][0]:
        i += 1
    lower_bound = i
    upper_bound = len(equidistant_angles)-1

    dt_eqAngles_indxBounds[valid_dtrs[-1]] = [lower_bound, upper_bound]
 
    return dt_eqAngles_indxBounds


################################
def get_angularZones_separators(dt_eqAngles_indxBounds):
    """
    """
    zone_seps = []

    for dt in dt_eqAngles_indxBounds.keys():
        sep1 =  angular_zone_separator(dt_eqAngles_indxBounds[dt][0], 'l', dt)
        sep2 =  angular_zone_separator(dt_eqAngles_indxBounds[dt][1], 'r', dt)
        zone_seps.append(sep1)
        zone_seps.append(sep2)

    #zone_seps = sorted(zone_seps, key=lambda sep: sep.val)
    return sorted(zone_seps, key=lambda sep: sep.val)
    
####################################
def dtrs_per_angular_zone(dt_angles, talking=False):
    """
    """
    
    angular_zones = {}
    present_dtrs = {}

    zone_seps = []

    for dt in dt_angles.keys():
        sep1 =  angular_zone_separator(dt_angles[dt][0], 'l', dt)
        sep2 =  angular_zone_separator(dt_angles[dt][-1], 'r', dt)
        zone_seps.append(sep1)
        zone_seps.append(sep2)

    zone_seps = sorted(zone_seps, key=lambda sep: sep.val)

    curr_dtrs = []
    for i in range(len(zone_seps)-1):
        angular_zones[i] = [ zone_seps[i].val, zone_seps[i+1].val ]
        if zone_seps[i].r_or_l == 'l':
            curr_dtrs.append(zone_seps[i].dt)
        else:
            curr_dtrs.remove(zone_seps[i].dt)
        present_dtrs[i] = curr_dtrs
        if talking: 
            print ('Angular zone '+ str(angular_zones[i]))
            print (' Dtrs seeing this zone: '+ str(present_dtrs[i]))
        
    return angular_zones, present_dtrs
    

def get_final_spectrum(rx2, dtrs_data, active_dtrs, 
                       #scaling_coeffs, 
                       shifts, talking=False):
    """
    calculate spectrum (x,y) = (equidist_angles, final_vals)
    return arrays equidist_angles and final_vals
    """
    if talking:
        print ('\nIn  get_final_spectrum:')
    # One day we will may be check the data in rder to say
    # if all the dtrs expected to function correctly do so.
    # That is why valid_dtrs array is defined after reading the data:
    valid_dtrs = active_dtrs
    #print valid_dtrs # debug
    
    uniform_step = (rx2[-1]-rx2[0])/len(rx2)
    if talking:
        print ('  uniform_step= '+ str(uniform_step))
    
    dt_angles = get_dt_angles(rx2, shifts)
    #print (str(dt_angles))
    
    
    #angular_zones, present_dtrs = dtrs_per_angular_zone(dt_angles) # debug !!!

    # Generate a uniform spectrum of rx2
    # on the whole anular range of the valid detectors.
    # Assumption that detectors are sorted in an increasing shift.
    # Where is this assumption needed ???
    equidist_angles = numpy.arange( dt_angles[valid_dtrs[0]][0],
                                    dt_angles[valid_dtrs[-1]][-1],
                                    uniform_step )
    #print len(equidist_angles)  # debug

    dt_eqAngles_indxBounds = get_dt_eqAngles_indxBounds(equidist_angles,
                                                        dt_angles)
    # --- debug ---
##        for dt in valid_dtrs:
##            print dt, ': (', dt_angles[dt][0], dt_angles[dt][-1], ')'
##            print ('[', equidist_angles[dt_eqAngles_indxBounds[dt][0]],
##                   equidist_angles[dt_eqAngles_indxBounds[dt][1]],']\n' )

    dt_interp_data = {}
    for dt in valid_dtrs:
        start_indx = dt_eqAngles_indxBounds[dt][0]
        end_indx = dt_eqAngles_indxBounds[dt][1]


##        interp_func = interp1d( dt_angles[dt], dtrs_data[dt-1] )
##        dt_interp_data[dt] = interp_func(equidist_angles[start_indx:end_indx+1])
##        print dt_interp_data[dt] # debug
##        # --- debug ---
##        numpy.savetxt(data_path + str(dt) + '.dat',
##                      numpy.transpose(numpy.array
##                                      ([equidist_angles[start_indx:end_indx+1],
##                                        dt_interp_data[dt]])),
##                      fmt = '%12.6g')

        if talking:
            print ('\n  equidist_angles in ['+str(equidist_angles[start_indx])+
                   ', '+str(equidist_angles[end_indx])+']')
            print ('  dt_angles['+str(dt)+']  in ['+str(dt_angles[dt][0])+
                   ', '+str(dt_angles[dt][-1])+']')
            
        dt_interp_data[dt] = numpy.array(
            linear_interpolation.interpolate(equidist_angles[start_indx:end_indx+1],
                        dt_angles[dt], dtrs_data[dt-1]))
##        print dt_interp_data[dt] # debug
##        # --- debug ---
##        numpy.savetxt(data_path + 'my_' + str(dt) + '.dat',
##                      numpy.transpose(numpy.array
##                                      ([equidist_angles[start_indx:end_indx+1],
##                                        dt_interp_data[dt]])),
##                      fmt = '%12.6g')

##    print ('\n  dt_interp_data['+str(valid_dtrs[-1])+'][-1]=',
##           dt_interp_data[valid_dtrs[-1]][-1])  # debug
        

    angularZones_sepIndxs = get_angularZones_separators(dt_eqAngles_indxBounds)
    present_dtrs = [min(valid_dtrs)]
    
    dt_currAngle_indx = {}
    for dt in valid_dtrs:
        dt_currAngle_indx[dt] = 0
            
    next_i = 1 
    next_zoneSep = angularZones_sepIndxs[next_i] # notation

    final_vals = numpy.zeros(len(equidist_angles))
    sigma_vals = numpy.zeros(len(equidist_angles))
    dtrs_nb = numpy.zeros(len(equidist_angles), dtype = int) # nb of detectors seeing each angle 
    #sigma_poisson = numpy.zeros(len(equidist_angles))
    
    for ai in range(len(equidist_angles)):
        
        next_zoneSep = angularZones_sepIndxs[next_i] # notation
        
        if ai == next_zoneSep.val and ai < len(equidist_angles)-1:

            if next_zoneSep.r_or_l == 'l':
                present_dtrs.append(next_zoneSep.dt)
            else:
                present_dtrs.remove(next_zoneSep.dt)
                
            if next_i < len(angularZones_sepIndxs)-1:
                next_i += 1
                next_zoneSep = angularZones_sepIndxs[next_i] # notation

                if ai == next_zoneSep.val:
                    if next_zoneSep.r_or_l == 'l':
                        present_dtrs.append(next_zoneSep.dt)
                    else:
                        present_dtrs.remove(next_zoneSep.dt)
                
                    if next_i < len(angularZones_sepIndxs)-1:
                        next_i += 1
                        next_zoneSep = angularZones_sepIndxs[next_i] # notation
                        
        dtrs_nb[ai] = len(present_dtrs)
        
        ai_vals = numpy.zeros(dtrs_nb[ai])
        idx = 0
        for dt in present_dtrs:
            ai_vals[idx] = dt_interp_data[dt][dt_currAngle_indx[dt]]
            final_vals[ai] += ai_vals[idx]
            idx += 1
            dt_currAngle_indx[dt] += 1
            
        if 1==1:  # options.normalize is True:
            final_vals[ai] /= dtrs_nb[ai]
            
        #sigma_poisson[ai] = scipy.sqrt(final_vals[ai]/len(present_dtrs)) # Eric
        
        #https://depts.washington.edu/imreslab/2011%20Lectures/ErrorProp-CountingStat_LRM_04Oct2011.pdf
        if dtrs_nb[ai] > 1:
            sigma_vals[ai] = numpy.sum((ai_vals[0:len(present_dtrs)]-final_vals[ai])**2)                
            sigma_vals[ai] = scipy.sqrt(sigma_vals[ai]/(dtrs_nb[ai]-1))
        else: 
            sigma_vals[ai] = scipy.sqrt(final_vals[ai])
            
        #sigma_vals[ai] = sigma_vals[ai]/scipy.sqrt(len(present_dtrs)) 
            
    if talking:
        print ('\n  Angular zones & present detectors:')
        angular_zones, present_dtrs = dtrs_per_angular_zone(dt_angles, talking)
        print ()
    
    return equidist_angles, final_vals, sigma_vals, dtrs_nb


def meta_data(normalized=True, bg_filename=None, shifts=None, scaling_coeffs=None):
    meta_data_str = ''
    
    if normalized:
        meta_data_str += 'normalized spectrum\n'
    else:
        meta_data_str += 'non normalized spectrum\n'
    meta_data_str += '\n'
    
    if bg_filename is not None:
        meta_data_str += 'background subtracted:\n'
        meta_data_str += os.path.abspath(bg_filename) +'\n'
    
    if shifts is not None:
        meta_data_str += '\n\tactive dtrs\tshift vals'
        if scaling_coeffs is not None:
            meta_data_str += '\tscaling factors'
        meta_data_str += '\n'
        
        for d in range(len(shifts)):
            meta_data_str += '# DT'+ str(d+1) +'\t\t'
            meta_data_str += str(shifts[d])
            if scaling_coeffs is not None:
                meta_data_str += '\t' + str(scaling_coeffs[d])
            meta_data_str += '\n'
        
    return meta_data_str+'\n'


##########################
if __name__ == '__main__':

    if len(sys.argv) < 2:
        print ("\nCorrect use: " + (sys.argv[0]).split('/')[-1] + " DATA_FILE [DT]")
        sys.exit(1)


    filename= sys.argv[1]
    
    splitted_data_path = filename.split(os.sep)
    data_path = ''
    if len(splitted_data_path) > 1:
        for s in splitted_data_path[:-1]:
            data_path += s + os.sep
    else:
        data_path = './'
        
    print (data_path) # debug

    print ("Reading data: "+ str(datetime.datetime.now()))
    rx2, dtrs_data = utils.reading_data(filename)
    print ("Reading data DONE: "+ datetime.datetime.now())
    
    average_step, stdev_step = angular_step_stats(rx2)
    uniform_step = (rx2[-1]-rx2[0])/len(rx2)
    print ('uniform_step ='+ uniform_step)
    print ('average_step ='+ average_step)
    print ('stdev_step ='+ stdev_step)
