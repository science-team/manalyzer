import os.path

from setuptools import setup


version = '0.0.2'
install_requires = ['numpy', 'scipy']
packages = ['manalyzer']

#package_data = {'extranormal.addons': ['imgs/*']}


setup(name='manalyzer',
      version=version,
      description='Tools for merging spectra issued from multi-analyser detector',
      author='Olga Roudenko',
      author_email='olga.roudenko@synchrotron-soleil.fr',
      license='GPL >= v.3',
      platforms=['Linux', 'Windows'],
      packages=packages,
      install_requires=install_requires
      #package_data=package_data,
      )
